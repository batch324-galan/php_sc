<?php 

class Person {
  protected $firstName;
  protected $middleName;
  protected $lastName;

  public function __construct($firstName, $middleName, $lastName) {
    $this->firstName = $firstName;
    $this->middleName = $middleName;
    $this->lastName = $lastName;
  }

  public function printName() {
    return "Your fullname is {$this->firstName} {$this->lastName}." . PHP_EOL;
  }
}

class Developer extends Person {
  public function printName() {
    return "Your name is {$this->firstName} {$this->middleName} {$this->lastName}." . PHP_EOL;
  }
}

class Engineer extends Person {
  public function printName() {
    return "You are an engineer named {$this->firstName} {$this->middleName} {$this->lastName}." . PHP_EOL;
  }
}

 ?>