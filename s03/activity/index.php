<?php require_once('./code.php'); ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S03 Activity</title>
	</head>
	<body>
		<h1>Person</h1>
		<p>
			<?php 
				$person = new Person("Juan", "", "Cruz");
				echo $person->printName();
			 ?>
		</p>

		<h1>Developer</h1>
		<p>
			<?php 
				$developer = new Developer("John", "Finch", "Smith");
				echo $developer->printName();
			 ?>
		</p>

		<h1>Engineer</h1>
		<p>
			<?php 
				$engineer = new Engineer("Harold", "Myers", "Reese");
				echo $engineer->printName();
			 ?>
		</p>
	</body>
</html>