<?php 
require_once('./code.php');
 ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Selection Control Structure</title>
	</head>
	<body>
		<h1>Array Manipulation</h1>
		<h2>Types of arrays</h2>

		<h3>Simple Arrays</h3>
		<ul>
			<?php foreach ($computerBrands as $brand) { ?>
				<li> <?= $brand;  ?> </li>
			<?php } ?>
		</ul>

		<h3>Associative arrays</h3>
		<ul>
			<?php foreach ($fruitsArr as $key => $value) {  ?>
			
			   <li><?= "$key is $value\n"; ?></li> 
			    
			<?php } ?>
		</ul>
	</body>
</html>