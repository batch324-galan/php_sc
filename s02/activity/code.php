<?php

$students = [];

$name = "John Smith";
$students[] = $name;

echo "Names added so far: ";
print_r($students);

$count = count($students);
echo "Count: " . $count . "<br/>";

$students[] = "Jane Doe";

echo "Updated array: ";
print_r($students);
echo "Count: " . count($students) . "<br/>";

array_shift($students);

echo "Updated array after removing the first student: ";
print_r($students);
echo "Count: " . count($students) . "<br/>";

?>