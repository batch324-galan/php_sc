<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02 Activity - Selection Control Structures and array Manipulation</title>
	</head>
	<body>
		
		<h1>Divisibles of 5</h1>
		<p>
		<?php

		$i = 0;
		while ($i <= 1000) {
		  if ($i % 5 == 0) {
		    echo $i . ' ';
		  }
		  $i++;
		}

		?>
		</p>

		<h1>Array Manipulation</h1>
		<p>
			<?php 
			include_once('./code.php');
			?>
		</p>
	</body>
</html>