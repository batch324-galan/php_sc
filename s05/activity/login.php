<?php
session_start();

$validUsername = 'johnsmith@gmail.com';
$validPassword = '1234';

if (isset($_SESSION['username'])) {
    echo 'Welcome, ' . $_SESSION['username'] . '!<br>';
    echo '<a href="logout.php">Logout</a>';
} else {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $username = $_POST['username'];
        $password = $_POST['password'];

        if ($username === $validUsername && $password === $validPassword) {
            $_SESSION['username'] = $username;
            header('Location: logged_in.php');
            exit;
        } else {
            echo 'Invalid username or password';
        }
    }
    ?>
    <form method="POST" action="">
        <label for="username">Username:</label>
        <input type="text" name="username" id="username" required><br>

        <label for="password">Password:</label>
        <input type="password" name="password" id="password" required><br>

        <input type="submit" value="Login">
    </form>
    <?php
}
?>