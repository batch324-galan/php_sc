<?php 


class Building {

	protected $name;
	protected $floors;
	protected $address;
	
	public function __construct($name, $floors,$address) {
		
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;

	}

	public function getName() {
		return $this->name;
	}

	public function setName($new_name) {
		$this->name = $new_name;
	}

	public function getFloors() {
		return $this->floors;
	}

	public function getAddress() {
		return $this->address;
	}

}


class Condominium extends Building {
	
}

$building = new Building('Caswyn Building', 9, 'Manila City, Manila');

$condominium = new Condominium('Enzo Condo', 100, 'Buendia Avenue, Makati City, Philippines');

 ?>