<?php require_once('./code.php'); ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s04 | Access modifiers and Encapsulation</title>
	</head>
	<body>
		<h1>Access Modifiers</h1>
		<p><?php var_dump($building); ?></p>
		<p><?php var_dump($condominium); ?></p>
		<p><?php echo $building->getName(); ?></p>
		<p><?php $building->setName('Sample Bldg');?></p>
		<p><?php echo $building->getName(); ?></p>

		<p><?php echo $condominium->getName(); ?></p>
	</body>
</html>