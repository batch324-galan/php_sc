<?php 


class Building {

	private $name;
	public $floors;
	public $address;
	
	public function __construct($name, $floors,$address) {
		
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;

	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
	  $this->name = $name;
	}
}

/**
 * 
 */
class Condominium extends Building {
	
}

$building = new Building('Trial Building', 9, 'Manila City, Manila');

$condominium = new Condominium('Trial Condominium', 100, 'Quezon City, Manila');

 ?>