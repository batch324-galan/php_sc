<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics and Selection Control Structure</title>
	</head>
	<body>
			<h1>Echoing Values</h1>
			<p><?php echo 'Good day $name! Your given email is $email';  ?></p>
			<p><?php echo "Good day $name! Your given email is $email";  ?></p>
			<p><?php echo PI ?></p>
			<p><?php echo PI2 ?></p>

			<h3>String</h3>
			<p><?php echo $state; ?></p>
			<p><?php echo $country; ?> </p>
			<p><?php echo $address; ?></p>

			<h3>Array</h3>
			<p><?php echo print_r($grades, true); ?></p> 
			<h3>Objects</h3>
			<p><?php echo $gradesObj->firstGrading; ?></p>
			<p>  </p>
	</body>
</html>