<?php

/*$trial = "Hello World!"; */

$name = 'John Smith';
$email = 'johnsmith@gmail.com';

// defining constants variable
const PI = 3.1416;
define('PI2', 3.1416);

//reassigning a variable
$name = 'Will Smith';

/*DATA TYPES*/
//strings 

$state = 'New York';
$country = 'United States of America';
$address = $state.', '.$country;
$address = "$state, $country.";

$grades = array(98.1, 87, 88, 90.1);

$gradesObj = (object) [
	'firstGrading' => 98.5,
	'secondGrading' => 97.3,
	'thirdGrading' => 98.2
]

?>