<?php 

function getFullAddress($country, $city, $province, $specificAddress) {

	$fullAddress = $specificAddress.', '.$city.', '.$province.', '.$country;
	return $fullAddress;

}


function getLetterGrade($grade) {
  $letterGrade = "";

  if ($grade >= 98 && $grade <= 100) {
    $letterGrade = "A+";
  } else if ($grade >= 95 && $grade <= 97) {
    $letterGrade = "A";
  } else if ($grade >= 92 && $grade <= 94) {
    $letterGrade = "A-";
  } else if ($grade >= 89 && $grade <= 91) {
    $letterGrade = "B+";
  } else if ($grade >= 86 && $grade <= 88) {
    $letterGrade = "B";
  } else if ($grade >= 83 && $grade <= 85) {
    $letterGrade = "B-";
  } else if ($grade >= 80 && $grade <= 82) {
    $letterGrade = "C+";
  } else if ($grade >= 77 && $grade <= 79) {
    $letterGrade = "C";
  } else if ($grade >= 75 && $grade <= 76) {
    $letterGrade = "C-";
  } else {
    $letterGrade = "D";
  }

  return $letterGrade;
}

 ?>