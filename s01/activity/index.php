<?php 

require_once('./code.php');

 ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01 Activity</title>
	</head>

	<body>

		<h1>Full Address</h1>
		<p><?php echo getFullAddress('Philippines', 'Mondragon', 'Northern Samar', 'Purok 2, Lagrimas Street' ) ?></p>

		<h1>Letter-Based Grading</h1>
		<p>
			<?php 
				$grade = 90;
				$letterGrade = getLetterGrade($grade);

				echo $grade . " is equivalent to " . $letterGrade;
			 ?>
		</p>

	</body>
</html>